SUMMARY
=======

The MaTrEx Exporter was developed to export source and translated files from
the TMGMT Linguaserve module.

This module grabs all necessary data, does a paragraph alignment of the source
and translated files and provides the files to be downloaded so it can be sent
to MaTrEx.

The goal of this is to train the MaTrEx Translation System.

REQUIREMENTS
============

Depends on Linguaserve Translator (linguaserve_translator)
  http://drupal.org/project/linguaserve_translator


Contract with Linguaserve is required to use the API from the module.

INSTALLATION
============

* Install the required module (Linguaserve Translator) as usual
  (http://drupal.org/node/70151).

* Install this module as usual, see http://drupal.org/node/70151
  for further information.

CONTACT
=======

Current maintainer:
* Karl Fritsche (kfritsche) - http://drupal.org/user/619702

This project has been sponsored by:
* Cocomore AG
  http://www.cocomore.com

* MultilingualWeb-LT Working Group
  http://www.w3.org/International/multilingualweb/lt/

* European Commission - CORDIS - Seventh Framework Programme (FP7)
  http://cordis.europa.eu/fp7/dc/index.cfm
